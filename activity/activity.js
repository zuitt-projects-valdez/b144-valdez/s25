//2.
db.fruits.aggregate([
  { $unwind: "$origin" },
  { $group: { _id: "$origin", stocksOfFruit: { $sum: "$stock" } } },
]);

// Solution: types of fruit and not stock
db.fruits.aggregate([
  { $unwind: "$origin" },
  { $group: { _id: "$origin", typesOfFruit: { $sum: 1 } } },
]);

//3.
db.fruits.aggregate([{ $match: { onSale: true } }, { $count: "onSale" }]);

//OR (if looking for total stock of onSale fruit)

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$onSale", totalStock: { $sum: "$stock" } } },
]);

//4.
db.fruits.aggregate([{ $match: { stock: { $gt: 20 } } }, { $count: "stock" }]);

//5.
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", avgPrice: { $avg: "$price" } } },
]);

//6.
db.fruits.aggregate([
  { $group: { _id: "$supplier_id", maxPrice: { $max: "$price" } } },
]);

//7.
db.fruits.aggregate([
  { $group: { _id: "$supplier_id", minPrice: { $min: "$price" } } },
]);
